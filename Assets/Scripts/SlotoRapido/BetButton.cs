using System;
using System.IO;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace SlotoRapido
{
    public class BetButton : MonoBehaviour
    {
        private static BetButton _instance;

        public BetButton()
        {
            _instance = this;
        }
        
        private TextMeshProUGUI _textMeshProUGUI;

        private static int _betValueIndex;

        private readonly int[] _increaseValues = new int[]
            {10, 25, 50, 100, 150, 250, 500, 1000, 1500, 2000, 5000, 10000, 25000, 50000};

        [SerializeField] private ButtonCustomInteractable minusButton;
        [SerializeField] private ButtonCustomInteractable plusButton;
        
        public static int BetValue => _instance._increaseValues[_betValueIndex];
        public static float BetMultiplier => (float) BetValue / 10;

        private void UpdateBetValue(int index = 0)
        {
            if (index < 0 || index >= _increaseValues.Length) return;
            _betValueIndex = index;
            _textMeshProUGUI.text = BetValue.ToString();
            minusButton.Interactable = _betValueIndex != 0;
            plusButton.Interactable = _betValueIndex != _increaseValues.Length - 1;
        }

        public void SetInteractable(bool value)
        {
            if (value)
            {
                UpdateBetValue(_betValueIndex);
                return;
            }

            minusButton.Interactable = false;
            plusButton.Interactable = false;
        }
        
        public void ChangeBetValue(int indexIncrement)
        {
            UpdateBetValue(_betValueIndex + indexIncrement);
        }
        
        private void Awake()
        {
            GetComponent<Button>().onClick.AddListener(Board.StartShuffle);
            _textMeshProUGUI = GetComponentInChildren<TextMeshProUGUI>();
            UpdateBetValue();
        }
    }
}
