using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace SlotoRapido
{
    public class Board : MonoBehaviour
    {
        #region SINGLETON

        private static Board _instance;

        private Board()
        {
            _instance = this;
        }

        #endregion
        
        #region PARAMETERS

        [SerializeField] private GameSettings gameSettings;
        private int WidthCount => gameSettings.widthCount;
        private int HeightCount => gameSettings.heightCount;
        
        
        
        [Header("Prefabs")] [SerializeField] private GameObject cellPrefab;
        private IEnumerable<SymbolAmount> SymbolAmounts => gameSettings.symbolAmounts;
        
        #endregion

        #region INIT
        
        private void Awake()
        {
            CreateCells();
        }

        private void CreateCells()
        {
            var rect = GetComponent<RectTransform>().rect;
            var cellSize = Mathf.Min(rect.width / WidthCount, rect.height / HeightCount);

            var sizeDelta = new Vector2(cellSize, cellSize);
            
            var x0 = (rect.width - cellSize * WidthCount) / 2;
            var y0 = (rect.height - cellSize * HeightCount) / 2;

            var t = transform;

            var cells = new Cell[WidthCount, HeightCount];
            
            for (var y = 0; y < HeightCount; y++)
            {
                for (var x = 0; x < WidthCount; x++)
                {
                    var cell = Instantiate(cellPrefab, t);
                    cell.GetComponent<RectTransform>().sizeDelta = sizeDelta;

                    cell.transform.localPosition = new Vector2(
                        x0 + (x + .5f) * cellSize,
                        y0 + (y + .5f) * cellSize);
                    cells[x, y] = cell.GetComponent<Cell>();

                    foreach (var index in NeighbourIndexes.LeftAndBottom)
                    {
                        var nx = x + index.X;
                        var ny = y + index.Y;

                        if (nx < 0 || nx >= WidthCount || ny < 0 || ny >= HeightCount) continue;

                        Cell.ConnectAsNeighbours(cells[x, y], cells[nx, ny]);
                    }
                }
            }
        }
        
        #endregion

        #region STATISTICS

        private class StatValues
        {
            public float Delta;
            public int Total;

            public StatValues()
            {
                this.Delta = 0;
                this.Total = 0;
            }
        }
        
        [Header("Statistic")]
        [SerializeField] private bool statisticMode;
        private Dictionary<string, StatValues> _statisticDictionary;
        [SerializeField] private int statisticGamesCount = 10;

        private void LaunchStatistic()
        {
            _statisticDictionary = new Dictionary<string, StatValues> {{"Nothing", new StatValues()}};


            var wildAmount = gameSettings.symbolAmounts.Where(
                symbolAmount => symbolAmount.symbol.IsWild()).Sum(symbolAmount => symbolAmount.amount);
            
            foreach (var symbolAmount in gameSettings.symbolAmounts)
                for (var i = Chain.MinValidChain; i < symbolAmount.amount + (symbolAmount.symbol.IsWild() ? 0 : wildAmount) + 1; i++)
                    _statisticDictionary.Add(symbolAmount.symbol.SymbolTypeName + "_" + i, new StatValues());
            
            for (var i = 0; i < statisticGamesCount; i++)
            {
                StartCoroutine(ShuffleCoroutineMethode());
                
                var w = new StreamWriter("Assets/Statistic/Progress.txt", false);
                w.WriteLine(i / statisticGamesCount);
                w.Close();
                
            }
            
            var writer = new StreamWriter("Assets/Statistic/Statistic.txt", false);

            writer.WriteLine("GAMES " + statisticGamesCount);
            writer.WriteLine("WIDTH " + gameSettings.widthCount);
            writer.WriteLine("HEIGHT " + gameSettings.heightCount);
            foreach (var symbolAmount in gameSettings.symbolAmounts)
            {
                writer.WriteLine(symbolAmount.symbol.SymbolTypeName + " " + symbolAmount.amount);
            }
            writer.WriteLine();
            
            foreach (var keyValuePair in _statisticDictionary)
            {
                writer.WriteLine(keyValuePair.Key + " " + keyValuePair.Value.Delta  + " " + keyValuePair.Value.Total);
            }
            
            writer.Close();
            
        }
        
        #endregion

        #region CURRENCY STATISTICS

        public bool currencyStatistics;

        private const int CurrencyStatisticMod = 25;
        private const int CurrencyStatisticInitialCoins = 100;
        
        private void LaunchCurrencyStatistics()
        {
            var writer = new StreamWriter("Assets/Statistic/CurrencyStatistic.txt", false);

            writer.WriteLine("GAMES " + statisticGamesCount);
            writer.WriteLine("WIDTH " + gameSettings.widthCount);
            writer.WriteLine("HEIGHT " + gameSettings.heightCount);
            foreach (var symbolAmount in gameSettings.symbolAmounts)
            {
                writer.WriteLine(symbolAmount.symbol.SymbolTypeName + " " + symbolAmount.amount);
            }
            writer.WriteLine();
            writer.Close();
            
            for (var i = 0; i < statisticGamesCount; i++)
            {
                Wallet.Coins = CurrencyStatisticInitialCoins;
                Wallet.Crystals = 0;
                var games = 0;
                while (Wallet.Coins >= 9)
                {
                    games += 1;
                    StartCoroutine(ShuffleCoroutineMethode());
                }
                
                writer = new StreamWriter("Assets/Statistic/CurrencyStatistic.txt", true);
                writer.WriteLine((Wallet.Crystals / CurrencyStatisticMod) * CurrencyStatisticMod + " " + games);
                writer.Close();
            }
            
        }

        #endregion
        
        #region GAME MANAGER

        public static void StartShuffle()
        {
            
            if (!_instance.statisticMode)
            {
                if(!_instance.currencyStatistics)
                {
                    _instance.StartCoroutine(_instance.ShuffleCoroutineMethode());
                    return;
                }
                _instance.LaunchCurrencyStatistics();
                return;
            }
            _instance.LaunchStatistic();
        }

        private const float ShuffleInterval = 0.05f;
        private const float FixInterval = 0.5f;
        
        private IEnumerator ShuffleCoroutineMethode()
        {
            Wallet.Coins -= BetButton.BetValue;
            if (_showResultsCoroutine != null)
            {
                StopCoroutine(_showResultsCoroutine);
                _showResultsCoroutine = null;
            }

            var cellsToShuffle = new List<Cell>(GetComponentsInChildren<Cell>());
            
            foreach (var cell in cellsToShuffle)
            {
                cell.StopWinAnimation();
            }
            
            var betButton = GameObject.Find("BetButton").GetComponent<ButtonCustomInteractable>();
            betButton.Interactable = false;

            ScoreFrame.SetState(ScoreFrame.ScoreFrameState.Shuffling);
            
            foreach (var cell in cellsToShuffle)
            {
                cell.SetSolid(false);
            }
            
            var symbolsToShuffle = new List<Symbol>();
            foreach (var symbolAmount in SymbolAmounts)
            {
                for (var i = 0; i < symbolAmount.amount; i++)
                {
                    symbolsToShuffle.Add(Instantiate(symbolAmount.symbol));
                }
            }

            var shuffleIndex = 0;

            while (cellsToShuffle.Count > 0)
            {
                var currentSymbolsToShuffle = new List<Symbol>(symbolsToShuffle);
                foreach (var cell in cellsToShuffle)
                {
                    var index = Random.Range(0, currentSymbolsToShuffle.Count);
                    cell.SetSymbol(currentSymbolsToShuffle[index]);
                    currentSymbolsToShuffle.RemoveAt(index);
                }

                if (shuffleIndex >= FixInterval / ShuffleInterval)
                {
                    for (var i = 0; i < WidthCount; i++)
                    {
                        var index = cellsToShuffle.Count - 1;
                        symbolsToShuffle.Remove(cellsToShuffle[index].CellSymbol);
                        cellsToShuffle[index].SetSolid(true);
                        cellsToShuffle.RemoveAt(index);
                    }


                    shuffleIndex = 0;
                }
                else
                {
                    shuffleIndex += 1;
                }

                if (!statisticMode && !currencyStatistics) yield return new WaitForSeconds(ShuffleInterval);
            }
            betButton.Interactable = true;
            
            var chains = Chain.FindChains(new List<Cell>(GetComponentsInChildren<Cell>()));

            ScoreFrame.SetState(chains.Count > 0 ? ScoreFrame.ScoreFrameState.Win : ScoreFrame.ScoreFrameState.NoWin);
            
            if(!statisticMode)
            {
                UpdateTotalScore(chains);
                if(!currencyStatistics)
                {
                    _showResultsCoroutine = StartCoroutine(ShowResultsCoroutineMethode(chains));
                }
            }
            else
            {
                if (chains.Count == 0)
                    _statisticDictionary["Nothing"].Total += 1;
                foreach (var chain in chains)
                {
                    _statisticDictionary[chain.SymbolTypeName + "_" + chain.Length].Delta += 1 / (float) chains.Count;
                    _statisticDictionary[chain.SymbolTypeName + "_" + chain.Length].Total += 1;
                }
            }

        }

        private Coroutine _showResultsCoroutine = null;
        
        private IEnumerator ShowResultsCoroutineMethode(List<Chain> chains)
        {
            
            var cells = new List<Cell>(GetComponentsInChildren<Cell>());

            var i = 0;
            
            while (chains.Count>0)
            {
                foreach (var cell in cells)
                {
                    var contains = chains[i].Contains(cell);
                    cell.SetSolid(contains);
                    if (contains)
                        cell.StartWinAnimation();
                    else
                        cell.StopWinAnimation();
                }
                i = i < chains.Count - 1 ? i + 1 : 0; 
                yield return new WaitForSeconds(2);
            }
        }

        #endregion

        #region SCORE VIEW

        [SerializeField] private Wallet wallet;
        [SerializeField] private CurrencyView totalCoinsView;
        [SerializeField] private CurrencyView totalCrystalsView;

        private void UpdateTotalScore(List<Chain> chains)
        {
            var score = new Score(0, 0);

            var betMultiplier = BetButton.BetMultiplier;
            
            foreach (var chain in chains)
            {
                score.Coins += Mathf.FloorToInt(chain.CurrentScore.Coins * betMultiplier);
                score.Crystals += Mathf.FloorToInt(chain.CurrentScore.Crystals * betMultiplier);
            }
            
            totalCoinsView.SetValue(score.Coins);
            totalCrystalsView.SetValue(score.Crystals);

            Wallet.Coins += score.Coins;
            Wallet.Crystals += score.Crystals;
        }

        #endregion
        
        #region GAME SETTINGS

        public static SymbolAmount GetSymbolAmount(Symbol symbol) =>
            _instance.SymbolAmounts.FirstOrDefault(
                instanceSymbolAmount => instanceSymbolAmount.symbol.SymbolTypeName == symbol.SymbolTypeName);


        #endregion
    }
}
