using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SlotoRapido
{
    public class ButtonCustomInteractable : MonoBehaviour
    {
        private Button _button;

        private static readonly Color disabledColor = new Color(1, 1, 1, 0.25f);
        
        private void Awake()
        {
            _button = GetComponent<Button>();
        }

        public bool Interactable
        {
            get => _button.interactable;
            set
            {
                if (_button != null)
                    _button.interactable = value;
                
                var color = value ? Color.white : disabledColor;
                
                foreach (var rawImage in GetComponentsInChildren<RawImage>())
                {
                    rawImage.color = color;
                }

                foreach (var textMeshProUGUI in GetComponentsInChildren<TextMeshProUGUI>())
                {
                    textMeshProUGUI.color = color;
                }

                var betButton = GetComponent<BetButton>();
                if (betButton == null) return;
                betButton.SetInteractable(value);
            }
        }
    }
}
