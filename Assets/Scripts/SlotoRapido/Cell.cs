using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace SlotoRapido
{
    public class Cell : MonoBehaviour
    {
        #region INIT

        private RawImage _iconRawImage;
        private GameObject _winBgGameObject;
        
        
        private void Awake()
        {
            foreach (var rawImage in GetComponentsInChildren<RawImage>())
            {
                switch (rawImage.name)
                {
                    case "Icon":
                        _iconRawImage = rawImage;
                        break;
                    case "WinBG":
                        _winBgGameObject = rawImage.gameObject;
                        break;
                }
            }
            StopWinAnimation();
            SetSolid(false);
        }

        #endregion
        
        #region NEIGHBOURS

        private readonly List<Cell> _neighbours = new List<Cell>();
        
        public static void ConnectAsNeighbours(Cell neighbour1, Cell neighbour2)
        {
            if (neighbour1 == null) return;
            if (neighbour2 == null) return;
            neighbour1.AddNeighbour(neighbour2);
            neighbour2.AddNeighbour(neighbour1);
        }

        private void AddNeighbour(Cell neighbour)
        {
            if (_neighbours.Contains(neighbour)) return;
            _neighbours.Add(neighbour);
        }

        #endregion

        #region SYMBOL

        private readonly Color _solidColor = Color.white;
        private readonly Color _transparentColor = new Color(1, 1, 1, 0.15f);
        
        public Symbol CellSymbol { get; private set; }

        public void SetSymbol(Symbol newSymbol)
        {
            if (newSymbol == null) return;
            if (CellSymbol == newSymbol) return;
            CellSymbol = newSymbol;
            _iconRawImage.texture = CellSymbol.texture;
        }

        public void SetSolid(bool solid)
        {
            _iconRawImage.color = solid ? _solidColor : _transparentColor;
        }
        
        #endregion

        #region CHAINING

        public Chain BuildChain(Chain chain = null)
        {
            if (chain != null && chain.Contains(this)) return chain;
            chain ??= new Chain(this);
            if (!IsMatchingChain(chain)) return chain;
            chain.AddCell(this);
            foreach (var neighbour in _neighbours)
            {
                neighbour.BuildChain(chain);
            }
            return chain;
        }

        private bool IsMatchingChain(Chain chain) =>
            CellSymbol.IsWild() || chain.SymbolTypeName == CellSymbol.SymbolTypeName;

        #endregion

        #region ANIMATION

        private const float WinAnimationAngle = 5;
        private static readonly Vector2 WinAnimationAlpha = new Vector2(0.6f, 1f);
        private const float WinAnimationSpeed = 5;

        private Coroutine _winAnimationCoroutine = null; 
        
        public void StartWinAnimation()
        {
            StopWinAnimation();
            _winAnimationCoroutine = StartCoroutine(WinAnimationCoroutineMethode());
        }

        public void StopWinAnimation()
        {
            _winBgGameObject.SetActive(false);
            _iconRawImage.transform.localRotation = Quaternion.identity;
            if (_winAnimationCoroutine == null) return;
            StopCoroutine(_winAnimationCoroutine);
            
        }

        private IEnumerator WinAnimationCoroutineMethode()
        {
            _winBgGameObject.SetActive(true);
            
            var t = _iconRawImage.transform;
            var seedAngle = Random.Range(0, 1000);
            var seedAlpha = Random.Range(0, 1000);
            var winBgRawImage = _winBgGameObject.GetComponent<RawImage>();
            
            while (true)
            {
                t.localRotation = Quaternion.Euler(
                    0,
                    0,
                    Mathf.Lerp(
                        -WinAnimationAngle, WinAnimationAngle,
                        Mathf.PerlinNoise((Time.time + seedAngle) * WinAnimationSpeed, 0)));

                winBgRawImage.color = new Color(
                    1, 1, 1,
                    Mathf.Lerp(
                        WinAnimationAlpha.x,
                        WinAnimationAlpha.y,
                        Mathf.PerlinNoise((Time.time + seedAlpha) * WinAnimationSpeed, 0)));
                
                yield return null;
            }
            
        }
       
        #endregion

    }
}
