
using System.Collections.Generic;
using UnityEngine;

namespace SlotoRapido
{
    public class Chain
    {
        public const int MinValidChain = 3;
        
        private List<Cell> _cells;
        private Symbol _symbol;
        public string SymbolTypeName => _symbol.SymbolTypeName;

        private bool _isWild;

        public int Length => _cells.Count;
        
        public Score CurrentScore { get; private set; }
        
        public Chain(Cell initialCell)
        {
            _cells = new List<Cell> {initialCell};
            _symbol = initialCell.CellSymbol;
            _isWild = initialCell.CellSymbol.IsWild();
        }

        public bool Contains(Cell cell) => _cells.Contains(cell);

        public void AddCell(Cell cell)
        {
            if(_cells.Contains(cell)) return;
            _cells.Add(cell);
        }

        public static List<Chain> FindChains(IEnumerable<Cell> cells)
        {
            var chains = new List<Chain>();
            var checkedCells = new List<Cell>();
            foreach (var cell in cells)
            {
                if(checkedCells.Contains(cell)) continue;
                
                var chain = cell.BuildChain();
                chain.InitScore();
                
                if (chain.CurrentScore.IsZero()) continue;
                
                chains.Add(chain);
                foreach (var chainCell in chain._cells)
                {
                    if(chainCell.CellSymbol.IsWild() && !chain._isWild) continue;
                    checkedCells.Add(chainCell);
                }
            }

            return chains;
        }

        private void InitScore()
        {
            CurrentScore = Board.GetSymbolAmount(_symbol).GetScore(_cells.Count);
            if (CurrentScore.IsZero()) return;
            //Debug.Log(_symbol.SymbolTypeName + "\nCoins: " + CurrentScore.Coins + " Crystals:" + CurrentScore.Crystals);
        }

    }
}
