using System;
using TMPro;
using UnityEngine;

namespace SlotoRapido
{
    public class CurrencyView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI textMeshPro;

        public enum CurrencyViewSyncType
        {
            NotSynced, Coins, Crystals
        }

        [SerializeField] private CurrencyViewSyncType syncType = CurrencyViewSyncType.NotSynced;
        
        private int _currentValue = 0;
        private int _targetValue = 0;

        private void Awake()
        {
            switch (syncType)
            {
                case CurrencyViewSyncType.Coins:
                    _currentValue = Wallet.Coins;
                    break;
                case CurrencyViewSyncType.Crystals:
                    _currentValue = Wallet.Crystals;
                    break;
            }
            textMeshPro.text = _currentValue.ToString();
            if (syncType == CurrencyViewSyncType.NotSynced) return;
            Wallet.WalletChangedEvent.AddListener(WalletChangedInvokeFunction);
        }

        public void SetValue(int value)
        {
            textMeshPro.text = value.ToString();
        }

        private void WalletChangedInvokeFunction()
        {
            _currentValue = syncType == CurrencyViewSyncType.Coins ? Wallet.Coins : Wallet.Crystals;
            textMeshPro.text = _currentValue.ToString();
        }
        
        
    }
}
