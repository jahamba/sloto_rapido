using System;
using System.Collections.Generic;
using UnityEngine;

namespace SlotoRapido
{
    [CreateAssetMenu(menuName = "Sloto Rapido/Symbol Amount Collection")]
    public class GameSettings : ScriptableObject
    {
        public int widthCount;
        public int heightCount;
        public List<SymbolAmount> symbolAmounts;
        
    }
}
