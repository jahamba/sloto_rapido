using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace SlotoRapido
{
    public class Menu : MonoBehaviour
    {
        [SerializeField] private List<MenuItem> items;
        [SerializeField] private GameObject menuItemPrefab;
        [SerializeField] private RectTransform selectionRectTransform;
        


        private float _selectedWidth;
        private float _commonWidth;
        private int _selectedIndex = 0;
        
        private void Awake()
        {
            UpdateItems(false);
            ClickEvent.AddListener(Clicked);
        }

        private void UpdateItems(bool coroutine = true)
        {

            
            
            for (var i = 0; i < items.Count; i++)
            {
                if (items[i].menuItemObject == null)
                {
                    items[i].menuItemObject = Instantiate(menuItemPrefab, transform).GetComponent<MenuItemPrefab>();
                    items[i].InitIconAndLabel(i);
                }

                items[i].menuItemObject.SetSelected(i == _selectedIndex);

                StartCoroutine(ChangeItemWidthCoroutine(i, coroutine));
            }
        }

        private const float TimeToChaneItem = 0.15f;
        
        private IEnumerator ChangeItemWidthCoroutine(int i, bool coroutine)
        {
            var delta = 1 / (float) (items.Count + 1);
            
            var rt = items[i].menuItemObject.GetComponent<RectTransform>();
            var leftAnchor = i * delta + (i > _selectedIndex ? delta : 0);
            var anchorMin = new Vector2(leftAnchor, 0);
            var anchorMax = new Vector2(leftAnchor + delta * (i == _selectedIndex ? 2 : 1), 1);
            
            var frameTransform = items[i].frameTransform;
            var frameTransformPosition = new Vector3(
                (i - _selectedIndex + 0.5f) * Screen.width,
                frameTransform.position.y,
                frameTransform.position.z);
            
            
            if (coroutine)
            {

                var dt = 0f;

                var oldAnchorMin = rt.anchorMin;
                var oldAnchorMax = rt.anchorMax;

                var oldFrameTransformPosition = frameTransform.position;

                var oldSelectionAnchorMin = selectionRectTransform.anchorMin;
                var oldSelectionAnchorMax = selectionRectTransform.anchorMax;
                
                while (dt < 1)
                {
                    rt.anchorMin = Vector2.Lerp(oldAnchorMin, anchorMin, dt);
                    rt.anchorMax = Vector2.Lerp(oldAnchorMax, anchorMax, dt);

                    if (i == _selectedIndex)
                    {
                        selectionRectTransform.anchorMin = Vector2.Lerp(oldSelectionAnchorMin, anchorMin, dt);
                        selectionRectTransform.anchorMax = Vector2.Lerp(oldSelectionAnchorMax, anchorMax, dt);
                    }
                    
                    frameTransform.position = Vector3.Lerp(
                        oldFrameTransformPosition,
                        frameTransformPosition,
                        dt);

                    dt += Time.deltaTime / TimeToChaneItem;
                    yield return null;
                }
            }

            rt.anchorMin = anchorMin;
            rt.anchorMax = anchorMax;
            if (i == _selectedIndex)
            {
                selectionRectTransform.anchorMin = anchorMin;
                selectionRectTransform.anchorMax = anchorMax;
            }
            
            frameTransform.position = frameTransformPosition;

        }

        private void Clicked(int index)
        {
            if (index == _selectedIndex) return;
            _selectedIndex = index;
            UpdateItems();
        }

        public static readonly UnityEvent<int> ClickEvent = new UnityEvent<int>();
    }
}
