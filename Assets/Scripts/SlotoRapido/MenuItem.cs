using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SlotoRapido
{
    [Serializable]
    public class MenuItem
    {
        public Texture[] icons;
        public string label;
        public MenuItemPrefab menuItemObject;

        public Transform frameTransform; 
        
        public void InitIconAndLabel(int index)
        {
            menuItemObject.InitIconAndLabel(index, icons, label);
            /*
            var w = Screen.width;

            frameTransform.transform.position = new Vector3(
                (index + 0.5f) * w,
                frameTransform.transform.position.y,
                frameTransform.transform.position.z);
            */
        }
        
    }
}
