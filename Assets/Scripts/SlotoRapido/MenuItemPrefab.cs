using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEditor;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SlotoRapido
{
    public class MenuItemPrefab : MonoBehaviour, IPointerClickHandler
    {
       [SerializeField] private RawImage iconRawImage;
       [SerializeField] private TextMeshProUGUI labelTextMeshProUGUI;

       private int _index;

       private Texture[] _icons;
       
       private readonly Dictionary<bool, Color> _colors = new Dictionary<bool, Color>
       {
           {true, Color.white},
           {false, new Color(1,1,1,0.5f)}
       };
       
        public void InitIconAndLabel(int i, Texture[] icons, string label)
        {
            _index = i;

            labelTextMeshProUGUI.text = label;
            //iconRawImage.texture = icon;
            _icons = icons;


        }

        private readonly Dictionary<bool, Vector2[]> minAnchors = new Dictionary<bool, Vector2[]>
        {
            {true, new[] {new Vector2(0, 0.3f), new Vector2(1, 1)}},
            {false, new[] {new Vector2(0, 0.2f), new Vector2(1, 0.8f)}}
        };
        
        
        
        public void SetSelected(bool selected)
        {
            labelTextMeshProUGUI.gameObject.SetActive(selected);
            iconRawImage.GetComponent<RectTransform>().anchorMin = minAnchors[selected][0];
            iconRawImage.GetComponent<RectTransform>().anchorMax = minAnchors[selected][1];
            iconRawImage.color = _colors[selected];
            labelTextMeshProUGUI.color = _colors[selected];
            iconRawImage.texture = _icons[selected ? 1 : 0];
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            Menu.ClickEvent.Invoke(_index);
        }
    }
}
