using UnityEngine;

namespace SlotoRapido
{
    public class Score
    {
        public int Coins;
        public int Crystals;

        public bool IsZero() => Coins == 0 && Crystals == 0;

        public Score(int coins, int crystals)
        {
            Coins = coins;
            Crystals = crystals;
        }

        public static readonly Score Zero = new Score(0, 0);
    }
}
