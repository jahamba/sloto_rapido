using System;
using UnityEngine;

namespace SlotoRapido
{
    public class ScoreFrame : MonoBehaviour
    {
        private static ScoreFrame _instance;

        public ScoreFrame()
        {
            _instance = this;
        }
        
        [SerializeField] private GameObject winFrame;
        [SerializeField] private GameObject shufflingFrame;
        [SerializeField] private GameObject noWinFrame;
        [SerializeField] private GameObject firstFrame;
        
        public enum ScoreFrameState
        {
            First, Shuffling, Win, NoWin
        }

        public static void SetState(ScoreFrameState state)
        {
            _instance.winFrame.SetActive(state == ScoreFrameState.Win);
            _instance.shufflingFrame.SetActive(state == ScoreFrameState.Shuffling);
            _instance.noWinFrame.SetActive(state == ScoreFrameState.NoWin);
            _instance.firstFrame.SetActive(state == ScoreFrameState.First);
        }

        private void Awake()
        {
            SetState(ScoreFrameState.First);
        }
    }
}
