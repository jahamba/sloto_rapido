using UnityEngine;

namespace SlotoRapido
{
    public abstract class Symbol : ScriptableObject
    {
        public Texture texture;
        private Cell _cell;

        public virtual bool IsWild() => false;
        
        public void SetParentCell(Cell cell)
        {
            _cell = cell;
        }

        public string SymbolTypeName => texture.name;
    }
}
