using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using UnityEngine;

namespace SlotoRapido
{
    [Serializable]
    public class SymbolAmount
    {
        public Symbol symbol;
        public int amount;
        [Multiline]
        [SerializeField] private string winConditons;

        private Dictionary<int, Score> _conditions;

        private Dictionary<int, Score> conditions
        {
            get
            {
                InitConditions();
                return _conditions;
            }
        }

        private void InitConditions()
        {
            if (_conditions != null) return;
            _conditions = new Dictionary<int, Score>();

            var stringReader = new StringReader(winConditons);

            var separator = new char[] {' '};
            
            while (true)
            {
                var line = stringReader.ReadLine();
                if (line == null) break;

                var values = line.Split(separator);
                _conditions.Add(
                    int.Parse(values[0], NumberStyles.Integer),
                    new Score(
                        int.Parse(values[1], NumberStyles.Integer),
                        int.Parse(values[2], NumberStyles.Integer)
                    ));
            }
        }

        public Score GetScore(int chainLength) =>
            !conditions.ContainsKey(chainLength) ? Score.Zero : conditions[chainLength];



    }
}
