using UnityEngine;
using UnityEngine.Events;

namespace SlotoRapido
{
    [CreateAssetMenu(menuName = "Sloto Rapido/Wallet")]
    public class Wallet : ScriptableObject
    {
        private static Wallet _instance;

        public Wallet()
        {
            _instance = this;
        }
        
        [SerializeField] private int coins;
        
        public static int Coins
        {
            get => _instance.coins;
            set
            {
                _instance.coins = value;
                WalletChangedEvent.Invoke();
            }
        }

        [SerializeField] private int crystals;

        public static int Crystals
        {
            get => _instance.crystals;
            set
            {
                _instance.crystals = value;
                WalletChangedEvent.Invoke();
            }
        }

        public static readonly UnityEvent WalletChangedEvent = new UnityEvent();
    }
}
