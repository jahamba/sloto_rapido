using UnityEngine;

namespace SlotoRapido
{
    [CreateAssetMenu(menuName = "Sloto Rapido/Wild Symbol")]
    public class WildSymbol : Symbol
    {
        public override bool IsWild() => true;
    }
}
